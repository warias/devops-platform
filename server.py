from flask import Flask
import mysql.connector
from datetime import date
from mysql.connector import errorcode
from sql_queries import *

# Flask app object
app = Flask(__name__)


# Endpoint


@app.route('/test')
def visitor_counter_test():
    counter_select = read_counter
    print("counter so far")
    return counter_select


@app.route('/')
def visitor_counter():
    # db connection to db container
    try:
        connection = mysql.connector.connect(user='root',
                                             password='LeoLucas86',
                                             host='db-svc',  # service name
                                             database='webapp')

        cursor = connection.cursor(buffered=True)
        cursor.execute(read_counter)
        counter_row = cursor.fetchone()
        if counter_row is None:
            counter_result = 1
            update_data = (counter_result, date.today())
            cursor.execute(insert_count, update_data)
            connection.commit()
        else:
            counter_row = list(counter_row)
            counter_result = counter_row[0]
            counter_result = counter_result + 1
            update_data = (counter_result, date.today())
            cursor.execute(insert_count, update_data)
            connection.commit()
        cursor.close()
        connection.close()

    except mysql.connector.Error as e:
        if (e.errno == errorcode.ER_ACCESS_DENIED_ERROR):
            print("WRONG USERNAME OR PASSWORD MY FRIEND !")
        elif e.errno == errorcode.ER_BAD_DB_ERROR:
            print("DATABASE DOES NOT EXIST BRUH ...")
        else:
            print(e)

    # cursor= db_cursor() weak reference, probaboy GC deleting the object

    # the endpoint is invoked, then we do a SELECT row from the db and insert after the reading
    return "you are visitor number: " + str(counter_result)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8081')
